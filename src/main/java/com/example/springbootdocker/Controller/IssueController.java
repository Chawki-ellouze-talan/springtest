package com.example.springbootdocker.Controller;

import com.example.springbootdocker.Entity.Issue;
import com.example.springbootdocker.Service.IssueService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/issues")
public class IssueController {

    private final IssueService issueService;


    public IssueController(IssueService issueService) {
        this.issueService = issueService;
    }

    @GetMapping
    public List<Issue> findAllIssues(){
        return issueService.getIssues();
    }
}
