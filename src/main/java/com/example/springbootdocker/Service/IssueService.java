package com.example.springbootdocker.Service;

import com.example.springbootdocker.Entity.Issue;
import com.example.springbootdocker.Repository.IssueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IssueService {

    private final IssueRepository issueRepository;


    @Autowired
    public IssueService(IssueRepository issueRepository) {
        this.issueRepository = issueRepository;
    }

    public List<Issue> getIssues(){
        return issueRepository.findAll();
    }
}
